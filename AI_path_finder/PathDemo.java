
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import java.util.*;
import java.time.format.*;
import java.time.*;
import ActionManager.ActionPanel;

/**
 * classe PathDemo
 * 
 * @author Stefano Giannini 
 * @version 22/01/19
 */

public class PathDemo implements Runnable
{   
    //random serial version
    private static final long serialVersionUID = 5041200123L; 
    private static int width = 800;
    private static int height = width * 10 / 16;
    private static int scale = 2;
    static final int FPS_MIN = 0;
    static final int FPS_MAX = 30;
    static final int FPS_INIT = 15; 
    private ActionPanel actionpanel;
    public short fps = 15;
    private boolean running = true;
    private JTextArea textArea;
    private JLabel generationLabel, timerLabel;
    private JFrame frame;
    JToolBar toolBar = new JToolBar();
   
    public Action runAction, stopAction, newGenerationAction;  
    //future implementation
    //slideAction,
    //private Thread t;
    //~ private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss");
    //~ private LocalTime mainTimer = LocalTime.MIN;
    
    /*
     * PathDemo's constructor
     */
    public PathDemo()
    {   
        //t = Thread.currentThread();
        //t.setName("First Thread");
        //t.setPriority(Thread.MAX_PRIORITY);
        actionpanel = new ActionPanel();
        initGUIComponents();
        createToolBar();
        actionpanel.initComponents();
        showGUI();
        run();
    }
    public void initGUIComponents(){
        
        
        textArea = new JTextArea(5, 30);
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);
        runAction = new RunAction(  "Train AI",
                                        "Click Here to start/continue Immagy's training.");
                                       
        stopAction = new StopAction("Pause Training", 
                                        "Click here to pause Immagy's training.");
                                       
        newGenerationAction = new NewGenerationAction("Force new Generation", 
                                        "Click here if some balls get stuck"); 
                                        //~ new Integer(KeyEvent.VK_S));
        //~ actionpanel = new JPanel() {
            //~ //Don't allow us to stretch vertically.
            //~ public Dimension getMaximumSize() {
                //~ Dimension pref = getPreferredSize();
                //~ return new Dimension(Integer.MAX_VALUE,
                                     //~ pref.height);
            //~ }
        //~ };
        
        actionpanel.add(scrollPane);
        actionpanel.setOpaque(true); //content panes must be opaque
        actionpanel.setMaximumCoordinates(width, height);
     }
     
     public void createToolBar() {
        JButton button = null;
        //Create the toolbar.
        actionpanel.add(toolBar, BorderLayout.PAGE_END);
        //start button
        button = new JButton(runAction);
        toolBar.add(button);
        //pause button
        button = new JButton(stopAction);
        toolBar.add(button);
        //new geneartion button
        button = new JButton(newGenerationAction);
        toolBar.add(button);
        toolBar.setFloatable(false);
        generationLabel = new JLabel(" Current Generation: " , JLabel.LEFT);
        toolBar.add(generationLabel);
        actionpanel.setToolBarOffset(62); //**Why Component.getHeight(doesn't work?)?**
        
    }
    //~ //add slider for fps modifier?
        //~ //Create the label.
        //~ JLabel sliderLabel = new JLabel("Frames Per Second", JLabel.CENTER);
        //~ sliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        //~ //Create the slider.
        //~ JSlider framesPerSecond = new JSlider(JSlider.HORIZONTAL,
                                              //~ FPS_MIN, FPS_MAX, FPS_INIT);
        
        //~ framesPerSecond.setMajorTickSpacing(10);
        //~ framesPerSecond.setMinorTickSpacing(1);
        //~ framesPerSecond.setPaintTicks(true);
        //~ framesPerSecond.setPaintLabels(true);
        //~ framesPerSecond.setBorder(
                //~ BorderFactory.createEmptyBorder(10,10,10,10));
        //~ Font font = new Font("Serif", Font.ITALIC, 15);
        //~ framesPerSecond.setFont(font);
        //~ toolBar.add(framesPerSecond);
        //~ toolBar.add(sliderLabel);
    

    public static int getWindowWidth() {
        return width * scale;
    }

    public static int getWindowHeight() {
        return height * scale;
    }
    
    public void showGUI(){
        
    frame = new JFrame("Shortest Path Finder - Immagy");
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(actionpanel);
        
        frame.pack();
        frame.setSize(width, height);
        frame.setVisible(true);
        frame.setResizable(false);
     }
     

     public void run(){
        try{
            while (true) {
                Thread.sleep(2);
                if(running){
                    //update timer(nope) and generation
                    generationLabel.setText(" Current Generation: " + Integer.toString(actionpanel.getCurrentGeneration())); //+ actionpanel.getCurrentGeneration()
                    //timerLabel.setText("        Timer: " + formatter.format(mainTimer.plusSeconds(actionpanel.timeLeft())));
                    actionpanel.update();
                    actionpanel.repaint();
                    if(actionpanel.getGoalReached()){
                        
                        int n = JOptionPane.showConfirmDialog(
                                                frame,
                                                "Your AI has just trained,\ndo you want to improve it?",
                                                "Improve AI ?",
                                                JOptionPane.YES_NO_OPTION);
                        if(n == 1){
                            running = false;
                        }
                        Thread.sleep(30);
                    }
                }
    
            }
        }
        catch (InterruptedException exc){
            exc.printStackTrace();
        }
    }
    
    /* 
     * Botton's actions to start/pause training
     */
    
    public class RunAction extends AbstractAction {
        public RunAction(String text, String desc) {
            super(text);
            putValue(SHORT_DESCRIPTION, desc);
        }
        public void actionPerformed(ActionEvent e) {
                System.out.println("Split the atom " );
                running = true;
        }
    }
    public class NewGenerationAction extends AbstractAction {
        public NewGenerationAction(String text, String desc) {
            super(text);
            putValue(SHORT_DESCRIPTION, desc);
        }
        public void actionPerformed(ActionEvent e) {
                System.out.println("New Generation is coming! " );                  
                actionpanel.evolveGenomes();
        }
    }

    public class StopAction extends AbstractAction {
        public StopAction(String text, String desc) {
            super(text); //if some time remains, I could add some images to these buttons
            putValue(SHORT_DESCRIPTION, desc);
        }
        public void actionPerformed(ActionEvent e) {
            System.out.println("Kick Stealth " );
            //~ Font font = new Font("Serif", Font.ITALIC, 13)
            //~ System.out.println(Integer.toString(n)); 0 for yes, 1 for no
            running = false;
        }
    }
    
    //controllo con eccezione se non si ha nessun file da caricare dell'AI o delle icone dei figli generati
    public static void main(String varargs[])
    {  
        PathDemo sample = new PathDemo();
    }
}
