package ActionManager;

import java.awt.Graphics2D;
import java.awt.*;
import javax.swing.*;

/*@Description
 * 
 * This abstract class define the skeleton of every drawable object
 */

public abstract class Drawable {
	//proteced for Ball class
	protected double x, y;
	
	public abstract void paint(Graphics2D g);
	
	public void setX(double x){
		 this.x = x;
	}
	
	public void setY(double y){
		this.y = y;
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
}
	
