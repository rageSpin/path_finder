package ActionManager;

import java.awt.Graphics2D;
import java.lang.*;
import javax.swing.*;
import java.awt.*;
import Algorithm.*;


public class Ball extends Drawable{
    
    //don't set final for future application/implementation
    public static int FEELER_COUNT = 4; 
    //~ private double x;
    //~ private double y;
    private NeuralNetwork neural;
    private ActionPanel aPanel;
    private int diameter;
    private Color randomColor = new Color((int)(Math.random() * 255), //ugly notation
                                (int)(Math.random() * 255),
                                 (int)(Math.random() * 255));
    private double[] normalizedDistances = new double[FEELER_COUNT];
    private double path = 0;
    private double angle;
    
    public Ball(ActionPanel aPanel, int diameter) {
        this.aPanel = aPanel;
        this.diameter = diameter;
    }

    /* 
     * update method, set the new input and get 
     * real-time output
     */ 
     
    public void update() {
        //constant ball speed and then set a direction rotation?
        //update until reach some checkpoint or final destination
        
            //when I understand more about sensor 
            //~ buildFeelers/Sensor;
            //~ detect distance between ball and obstacles through Feelers/Sensors;
            normalizeTheDistances();
        try{
            //System.out.println("Before the dead");
            neural.setInput(normalizedDistances);
            neural.update();
            //using the enumaration movement options
            double rightDirection = 2 * neural.getOutput(ActionPanel.NeuralNetOuputs.NN_OUTPUT_RIGHT_DIRECTION.ordinal());
            double leftDirection = -2 * neural.getOutput(ActionPanel.NeuralNetOuputs.NN_OUTPUT_LEFT_DIRECTION.ordinal());
            double upDirection = -2 * neural.getOutput(ActionPanel.NeuralNetOuputs.NN_OUTPUT_UP_DIRECTION.ordinal());
            double downDirection = 2 * neural.getOutput(ActionPanel.NeuralNetOuputs.NN_OUTPUT_DOWN_DIRECTION.ordinal());
                                    
            //normalize from -1 to 1
            x = x + rightDirection + leftDirection; 
            y = y + upDirection + downDirection;
                    
            
            path += Math.sqrt(Math.pow((rightDirection + leftDirection), 2) + Math.pow((upDirection + downDirection), 2));
            //~ System.out.println("Test normalizedOutput up " + Double.toString(upDirection));
            //~ System.out.println("Test normalizedOutput leftDirection " + Double.toString(leftDirection));
            //~ System.out.println("Test normalizedOutput rightDirection " + Double.toString(rightDirection));
            //~ System.out.println("Test normalizedOutput downDirection " + Double.toString(downDirection));
            //System.out.println("Test normalizedDistances angle " + Double.toString(angle));
            //System.out.println("Test normalizedDistances yDistance " + Double.toString(normalizedDistances[3]));
            //System.out.println("Test normalizedDistances sin " + Double.toString(normalizedDistances[0]));
            //System.out.println("Test normalizedDistances cos " + Double.toString(normalizedDistances[1]));
            
            //System.out.println("Asserzione shortest path " + Double.toString(path));
        }catch(NullPointerException exc){
            exc.getCause();
            exc.printStackTrace();
            
        }
    }
    
    
    public boolean isCollided(){
        if (x < 0)
            return true;
        if (x > aPanel.getWidth() - diameter)
            return true;
        if (y < 0)
            return true;
            //~ y++;
        if (y > aPanel.getHeight() - 2 * diameter - 2) //-2 for some borderLayout bug 
            return true;
            //~ y--;
        return false;
    }
    
    
    public double getPath(){
        return path;
    }
    
    public void normalizeTheDistances(){
        
        angle = Math.acos(getBallXDistance() / getBallDistance());
    
        if(getBallYDistance() < 0){
            angle = Math.PI * 2 - angle;
            //~ normalizedDistances[0] = Math.pow(Math.E, -(angle - Math.PI / 2) * (angle - Math.PI / 2));
            //~ normalizedDistances[1] = Math.pow(Math.E, -(angle - Math.PI) * (angle - Math.PI));
            //~ normalizedDistances[2] = Math.pow(Math.E, -(angle ) * (angle ));
            //~ normalizedDistances[3] = Math.pow(Math.E, -(angle + Math.PI / 2) * (angle + Math.PI / 2));
        }
        
        normalizedDistances[0] = Math.sin(angle);
        normalizedDistances[1] = Math.cos(angle);
        normalizedDistances[2] = getBallX() / (aPanel.finalBasePoint.getX() - aPanel.startBasePoint.getX());
        normalizedDistances[3] = getBallY() / (aPanel.finalBasePoint.getY() - aPanel.startBasePoint.getY());                
    }
    
    public Color getBallColor(){
            return randomColor;
        }
        
    @Override   
    public void paint(Graphics2D g) {
        g.fillOval((int)x, (int)y, diameter, diameter); 
    }
    
    public void attach(NeuralNetwork neuralNet) {
        this.neural = neuralNet;
    }
    
    public void setStartPosition(double startPositionX, double startPositionY){
        x = startPositionX;
        y = startPositionY;
    }
        //I can do this with Actionpanel (game)
    public double getBallDistance(){
        double squaredDistance = Math.pow(getBallXDistance(), 2) + Math.pow(getBallYDistance(), 2);
        return Math.sqrt(squaredDistance);
    }
    
    public double getFinalDistance(){
        double squaredDistance = Math.pow(aPanel.finalBasePoint.getX() - aPanel.startBasePoint.getX(), 2) 
                                + Math.pow(aPanel.finalBasePoint.getY() - aPanel.startBasePoint.getY(), 2);
        return Math.sqrt(squaredDistance);
    }
    
    public double getBallXDistance(){
        return (aPanel.finalBasePoint.getX() - x);
    }
    public double getBallYDistance(){
        return (aPanel.finalBasePoint.getY() - y);  
    }
    
    public double getBallX(){
        return (x - aPanel.startBasePoint.getX());
    }
    public double getBallY(){
        return (y - aPanel.startBasePoint.getY());
    }
    
    
    
}
