package ActionManager;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import Algorithm.*;

/*@Description
 * 
 * This JPanel class manages all the Genesis algorithm methods and instances.
 */

public class ActionPanel extends JPanel //implements ItemListener
{
    private static int maxX, maxY;
    private static final int HIDDEN_LAYER_NEURONS = 8;
    private static final int MAX_GENOME_POPULATION = 15; 
    private static final int NN_OUTPUT_COUNT = 4;
    private static final short BALL_DIAMETER = 30;
    private static final Color START_COLOR = new Color(0, 0, 0);
    private static final Color FINAL_COLOR = new Color(0, 200, 50);
    private int toolBarOffset;
    //maybe a listenerArrayList
    private ArrayList<Ball> ballList = new ArrayList<Ball>(MAX_GENOME_POPULATION); 
    private ArrayList<NeuralNetwork> neuralNets;
    private GeneticAlgorithm genAlg;
    //~ private long initTimer;
    //~ private long elapsedTime = 0L;
    private boolean trainReached = false;
    private short ballsCollided = 0;
    private short ballsArrived = 0;
    //maybe there is a more efficient way, byte array?
    private boolean[] collisionStates = new boolean[MAX_GENOME_POPULATION];
    private boolean[] goalStates = new boolean[MAX_GENOME_POPULATION];
    double midDistance;
    double currentDistanceFitness;
    double currentPathFitness;
    BasePoint startBasePoint;
    BasePoint finalBasePoint; 
    
    //enumeration available for every class' istances
    public static enum NeuralNetOuputs { 
        NN_OUTPUT_RIGHT_DIRECTION, NN_OUTPUT_LEFT_DIRECTION, NN_OUTPUT_UP_DIRECTION, NN_OUTPUT_DOWN_DIRECTION,
    }
   
    public ActionPanel(){
        
        //initialize balls here
        super(new BorderLayout()); 
        startBasePoint = new BasePoint(START_COLOR);
        startBasePoint.setX(0);
        startBasePoint.setY(0);
        finalBasePoint = new BasePoint(FINAL_COLOR);
        for(short i = 0; i < MAX_GENOME_POPULATION; i++) {
            ballList.add(new Ball(this, BALL_DIAMETER));    
            ballList.get(i).setStartPosition(startBasePoint.getX(), startBasePoint.getY());
            collisionStates[i] = false;
            goalStates[i] = false;
        }
        
        
    }
    
    /*
     * initialize other components 
     */ 
    public void initComponents() {
        
        //"-3" because of borderLayout without a getter method
        finalBasePoint.setX(maxX - 3 - BALL_DIAMETER);
        finalBasePoint.setY(maxY - (toolBarOffset + BALL_DIAMETER)); 
        //initialize algortithm Weights and Network
        int totalWeights = (ballList.get(0).FEELER_COUNT) * HIDDEN_LAYER_NEURONS
                + HIDDEN_LAYER_NEURONS * NN_OUTPUT_COUNT + HIDDEN_LAYER_NEURONS
                + NN_OUTPUT_COUNT;
        
        
        //I can pass to Genetic Algorithm everything I need to build better performance
        midDistance = ballList.get(0).getBallDistance() / 2; 
        //~ System.out.println("Asserzione midDistance " + Double.toString(midDistance));
        genAlg = new GeneticAlgorithm(ballList.get(0).getFinalDistance());
        genAlg.generateNewPopulation(MAX_GENOME_POPULATION, totalWeights);
        neuralNets = new ArrayList<NeuralNetwork>();
                
        giveBrain();
    }
    
   
    
    @Override
    public void paint(Graphics g) {
        //for more efficiency It could be used two layers, one with the ball's movements, the other with BasePoint
        super.paint(g); 
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        
        startBasePoint.paint(g2d);
        finalBasePoint.paint(g2d);
        
        for(short i = 0; i < MAX_GENOME_POPULATION; i++) {
            if(!collisionStates[i]){
                g2d.setColor(ballList.get(i).getBallColor());
                ballList.get(i).paint(g2d);
            }
        }

    }   
    
    //each generation has more time to find the right path
     //insert Timer from main?
    //~ protected long timeLeft(){
        
        //~ LocalTime timer = initTimer.plusSeconds(100 + genAlg.getCurrentGeneration() * 2);
        //~ //System.out.println(timer.toString());
        //~ return elapsedTime = (new Date()).getTime() - initTimer;
    //~ }
    
    //~ protected boolean isTimeUp(){
        //~ if(timeLeft() >= (8 + genAlg.getCurrentGeneration() * 2) * 1000){
            //~ //System.out.println(Long.toString(timeLeft()));
            //~ initTimer = System.currentTimeMillis();
            //~ return true;
        //~ }
        //~ else 
            //~ return false;
    //~ }
    
    
    public void giveBrain() {
        
        //attach the neural network to balls
        for(short i = 0; i < MAX_GENOME_POPULATION; i++){
            neuralNets.add(new NeuralNetwork());
            Genome genome = genAlg.getNextGenome();
            neuralNets.get(i).fromGenome(genome, ballList.get(0).FEELER_COUNT,
                HIDDEN_LAYER_NEURONS, NN_OUTPUT_COUNT);
            ballList.get(i).attach(neuralNets.get(i));
            //System.out.println("Asserzione balls dichiarate " + Double.toString(ballList.get(i).neural.getOutput(1)));
        }
        //bestFitness = ballList.get(0).getBallDistance();
    }
    
    public void bestPath(){
        if(ballsArrived >=1){   
            double bestFitness = 3 * midDistance;
            short bestIndex = 0;
            for(short i = 0; i < MAX_GENOME_POPULATION; i++) {
                if(goalStates[i]){
                   currentPathFitness = ballList.get(i).getPath();
                   if(currentPathFitness < bestFitness){
                       bestFitness = currentPathFitness;
                       bestIndex = i;
                    }
                }
            }
        //set 0 distance to make it the best genome
        genAlg.setGenomeFitness(0, bestIndex);
        }
    }
    
    public void evolveGenomes() {
        bestPath();
        genAlg.breedPopulation();
        ballList.clear();
        neuralNets.clear();
        for(short i = 0; i < MAX_GENOME_POPULATION; i++) {
            ballList.add(new Ball(this, BALL_DIAMETER));    
            ballList.get(i).setStartPosition(startBasePoint.getX(), startBasePoint.getY());
            collisionStates[i] = false;
            goalStates[i]    = false;
        }
        ballsCollided = 0;
        ballsArrived = 0;
        giveBrain();
    }
    
    //when the ball reach the goal, reward more ?
    public void update() {
        
        //~ System.out.println("Asserzione midDistance " + Double.toString(midDistance));
        
        for(short i = 0; i < MAX_GENOME_POPULATION; i++){
            //~ if (ballList.get(i).getBallDistance() >= (2 * midDistance * (30 - genAlg.getCurrentGeneration()) / 30)) {
            if((ballsCollided + ballsArrived) < MAX_GENOME_POPULATION) {
                
                //circular checkpoint to set a performance minitask?
                if(!collisionStates[i] && !goalStates[i]){
                    ballList.get(i).update();
                    if(ballList.get(i).getBallDistance() <= 3){
                        goalStates[i] = true;
                        ballsArrived++;
                    }
                    if(ballList.get(i).isCollided()){
                        collisionStates[i] = true;
                        ballsCollided++;
                    }
                    currentDistanceFitness = ballList.get(i).getBallDistance();
                    //currentPathFitness = ballList.get(i).getPath();
                    System.out.println("Distance + Path " + Double.toString(currentDistanceFitness));
                    genAlg.setGenomeFitness(currentDistanceFitness, i);
                }

            }
            else{
                if(ballsCollided >= MAX_GENOME_POPULATION || ballList.get(i).getPath() >= 2 * midDistance + 3){
                    //exit for cycle
                    i = MAX_GENOME_POPULATION;
                    evolveGenomes();
                }
                else{
                        trainReached = true;
                        i = MAX_GENOME_POPULATION;
                        randomizeBasePoints();        
                }
            }
        }
    }
    
    
    
    public void randomizeBasePoints(){
        //It could be done with an array but in this way names are more significative
        int randomStartX = (int)(Math.random() * ( maxX - 3 - BALL_DIAMETER ));
        int randomStartY = (int)(Math.random() * (maxY - (toolBarOffset + BALL_DIAMETER)));
        int randomFinalX = (int)(Math.random() * (maxX - 3 - BALL_DIAMETER));
        int randomFinalY = (int)(Math.random() * (maxY - (toolBarOffset + BALL_DIAMETER)));
        
        
        if(randomStartX != randomFinalX && randomStartY != randomFinalY){
            finalBasePoint.setX(randomFinalX);
            finalBasePoint.setY(randomFinalY);
            startBasePoint.setX(randomStartX);
            startBasePoint.setY(randomStartY);
            evolveGenomes();
            //~ for(short i = 0; i < MAX_GENOME_POPULATION; i++){
                //~ ballList.get(i).setStartPosition(randomStartX, randomStartY);
            //~ }
            genAlg.setNewFitnessParameter(ballList.get(0).getBallDistance());
            midDistance = ballList.get(0).getBallDistance() / 2; 
            
        }
        else 
            randomizeBasePoints();  
    }
     
     
    public int getCurrentGeneration(){
        //redundant method to pass current generation to main program;
        return genAlg.getCurrentGeneration();
    }
    public void setToolBarOffset(int toolBarOffset){
        this.toolBarOffset = toolBarOffset;
    }
    
    public void setMaximumCoordinates(int maxX, int maxY){
        this.maxX = maxX;
        this.maxY = maxY;
    }
    
    public boolean getGoalReached(){
        if(trainReached && genAlg.getCurrentGeneration() >= 70){
            trainReached = false;
            return true;
        }
        else{
            
            return false;
        }
    }
    
}
