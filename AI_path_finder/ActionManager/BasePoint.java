package ActionManager;

import java.awt.Graphics2D;
import java.awt.*;
import javax.swing.*;

public class BasePoint extends Drawable{ 
	//~ private int x;
	//~ private int y;
	//variabili di grandezza rectangle e di bordi.
	private Color color;
	
	public BasePoint(Color color){
		this.color = color;
	}
	
	@Override
	public void paint(Graphics2D g) {
		g.setColor(color);
		g.fillRect((int)x, (int)y, 30, 30); 
	}
	
	
}
