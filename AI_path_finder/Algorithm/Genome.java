package Algorithm;

import java.util.ArrayList;

/*
 * @Description: Genome object simply keeps 2 important info: fitness and weights. 
 * The fitness is the distance that how long the ball could go before end of Timer. The weights are
 * a list of Sigmoid values which is from -1 to 1.
 * Each ball has his Genome?
 */
public class Genome {
	public int ID;
	public double fitness;
	public ArrayList<Double> weights;
}
