package Algorithm;

import java.util.ArrayList;

/*
 * @Description: NeuralNetwork is used to make decision for the Balls; decide that one of them
 * should go. These layers will constantly update to get new values from genomes
 * each time the time is out or some  crashs to the wall. The network will use new values from Genetic to make
 * decision for the next try of balls. 
 */
public class NeuralNetwork {
	private int inputAmount;
	private int outputAmount;
	private ArrayList<Double> outputs;
	private ArrayList<Double> inputs;
	// HiddenLayer produces the input for OutputLayer
	private ArrayList<NeuronsLayer> hiddenLayers;
	// OutputLayer will receive input from HiddenLayer
	private NeuronsLayer outputLayer;
	
	public NeuralNetwork() {
		outputs = new ArrayList<Double>();
		inputs = new ArrayList<Double>();
	}

	/*
	 * receive input distance from the ball which from startingPoint of fillOval g2d method
	 */
	public void setInput(double[] normalizedDistances) {
		inputs.clear();
		
		for (short i = 0; i < normalizedDistances.length ; i++) {
			//~ System.out.println("Distances set: " + Double.toString(normalizedDistances[i]));
			inputs.add(normalizedDistances[i]);
		}
	}

	@SuppressWarnings("unchecked") //for inputs/ outputs evaluation
	public void update() {
		outputs.clear();
		for (int i = 0; i < hiddenLayers.size(); i++) {
			if (i > 0) {
				inputs = outputs;
			}
			// each hidden layer calculates the outputs based on inputs
			hiddenLayers.get(i).evaluate(inputs, outputs);
			System.out.println("Output of hiddden layers: "
					+ outputs.toString());
		}
		// the outputs of HiddenLayers will be used as input for
		// OutputLayer
		inputs = (ArrayList<Double>) outputs.clone();
		// The output layer will give out the final outputs
		outputLayer.evaluate(inputs, outputs);
	}

	public double getOutput(int index) {
		//add exception with try, catch?
		if (index >= outputAmount)
			return 0.0f;
		return outputs.get(index);
	}

	
	public void releaseNet() {
		// inputLayer = null;
		outputLayer = null;
		hiddenLayers = null;
	}

	/*
	 * Neural network receives weights from genome to make new HiddenLayers and
	 * OutputLayer.
	 */
	public void fromGenome(Genome genome, int numOfInputs,
			int neuronsPerHidden, int numOfOutputs) {
		if (genome == null)
			return;
		releaseNet();
		hiddenLayers = new ArrayList<NeuronsLayer>();
		outputAmount = numOfOutputs;
		inputAmount = numOfInputs;
		NeuronsLayer hidden = new NeuronsLayer();
		ArrayList<Neuron> neurons = new ArrayList<Neuron>();
		try{
			for (int i = 0; i < neuronsPerHidden; i++) {
				ArrayList<Double> weights = new ArrayList<Double>();
				for (int j = 0; j < numOfInputs + 1; j++) { //+1
					weights.add(genome.weights.get(i * neuronsPerHidden + j));
				}
				Neuron n = new Neuron();
				n.init(weights, numOfInputs);
				neurons.add(n);
			}
		}catch(IndexOutOfBoundsException exc){
			exc.printStackTrace();
		}
		hidden.loadLayer(neurons);
		hiddenLayers.add(hidden);

		// Clear weights and reasign the weights to the output.
		ArrayList<Neuron> neuronsOut = new ArrayList<Neuron>();
		for (int i = 0; i < numOfOutputs; i++) {
			ArrayList<Double> weights = new ArrayList<Double>();
			for (int j = 0; j < neuronsPerHidden + 1; j++) {
				weights.add(genome.weights.get(i * neuronsPerHidden + j));
			}
			Neuron n = new Neuron();
			n.init(weights, neuronsPerHidden);
			neuronsOut.add(n);
		}
		outputLayer = new NeuronsLayer();
		outputLayer.loadLayer(neuronsOut);
	}

}
