package Algorithm;

import java.util.ArrayList;

/*
 * @Description: Neuron is the basic element in the neuron network. Each neuron has
 * a certain number of inputs. In the first version of this program we have only a distance-sensor from finalBasePoint, 
 * then 5 sensor to recognize hurdles.
 */

public class Neuron {
	protected int numberOfInputs;
	protected ArrayList<Double> weights;

	public void init(ArrayList<Double> weightsIn, int numOfInputs) {
		this.numberOfInputs = numOfInputs;
		weights = weightsIn;
	}
}
