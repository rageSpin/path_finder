----------------------------------------------------------------------------------
******** File README del progetto ********  
Progetto AI rudimentale per imparare le basi del "pattern recognition" e "genesis algorithm". Non si sono ancora provate forme più avanzate di intelligenza artificiale. Ciò è stato presentato ad un esame presso l'Università di Fisica a Torino.

----------------------------------------------------------------------------------

NOME DEL PROGETTO:Immagy
OBIETTIVO DEL PROGETTO: Creare un'intellegenza artificiale che scelga impari a muoversi e a scegliere il percorso più breve.
Seconda versione, inserire ostacoli e vedere come reagisce (aggiungendo dei sensori di prossimità)
Terza versione: implementare miglior percorso all'interno di una rete
VERSIONE oppure DATA: Versione 1 15/01/2019
COME AVVIARE IL PROGETTO: eseguire il file Jar o compilando codice sorgente con:
     "javac *.java" e poi "java PathDemo"
AUTORI: Stefano Giannini, con la collaborazione di progetti open-source. 
ISTRUZIONI PER L'UTENTE:
1) Allena la tua AI con "Train AI"
2) Metti in pausa
3) Forza la generazione di nuovi genomi a seconda delle tue necessità.

Grazie di aver scaricato questo progetto